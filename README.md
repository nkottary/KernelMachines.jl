# KernelMachines

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://VEOS-research.gitlab.io/KernelMachines.jl/dev)
[![Build Status](https://gitlab.com/VEOS-research/KernelMachines.jl/badges/master/pipeline.svg)](https://gitlab.com/VEOS-research/KernelMachines.jl/pipelines)
[![Coverage](https://gitlab.com/VEOS-research/KernelMachines.jl/badges/master/coverage.svg)](https://gitlab.com/VEOS-research/KernelMachines.jl/commits/master)

Julia implementation of discrete kernel machines. The theoretical foundations are discussed in the [parametric machines](https://arxiv.org/abs/2007.02777) article, in particular, see section 4 for kernel machines. Here, we only implement the discrete case (section 4.2).

:warning: This package requires Julia 1.5.

See the [documentation](https://VEOS-research.gitlab.io/KernelMachines.jl/dev) or the [examples folder](https://gitlab.com/VEOS-research/KernelMachines.jl/-/tree/master/examples) to get started.

